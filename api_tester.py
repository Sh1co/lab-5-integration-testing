import logging
from json.decoder import JSONDecodeError
from dataclasses import dataclass, asdict, replace
from typing import Literal, List, Union

import requests

PRICE_API_URL = "https://script.google.com/macros/s/AKfycbxReuav_j4FMBmT7i6ozd5E9oVjBOC7pKFyp5VlKjTRqmn7hjoeTDuzSayzMzTKKi2W/exec?service=calculatePrice&email=s.nafee@innopolis.university"

logging.basicConfig()

# Spec for s.nafee@innopolis.university
spec = {
    "budget_price_per_min": 30,
    "luxury_price_per_min": 83,
    "fixed_price_per_km": 16,
    "allowed_deviation": 8,
    "inno_discount": 10,
}


print("Spec:", spec)


@dataclass
class TestCase:
    type: Literal["budget", "luxury", "nonsense"]
    plan: Literal["minute", "fixed_price", "nonsense"]
    distance: int
    planned_distance: int
    time: int
    planned_time: int
    inno_discount: Literal["yes", "no", "nonsense"]

    @property
    def is_valid(self: "TestCase") -> bool:
        if self.type not in ["budget", "luxury"]:
            return False
        if self.plan not in ["minute", "fixed_price"]:
            return False
        if self.plan == "fixed_price" and self.type != "budget":
            return False
        if self.distance <= 0:
            return False
        if self.planned_distance <= 0:
            return False
        if self.time <= 0:
            return False
        if self.planned_time <= 0:
            return False
        if self.inno_discount not in ["yes", "no"]:
            return False
        return True


def expected_price(test_case: TestCase) -> Union[Literal["Invalid Request"], int]:
    if not test_case.is_valid:
        return "Invalid Request"
    if test_case.plan == "fixed_price":
        deviation_dist = (
            (test_case.planned_distance - test_case.distance)
            / test_case.planned_distance
            * 100
        )
        deviation_time = (
            (test_case.planned_time - test_case.time) / test_case.planned_time * 100
        )
        if (
            deviation_dist > spec["allowed_deviation"]
            or deviation_time > spec["allowed_deviation"]
        ):
            test_case = replace(test_case, plan="minute")
        else:
            price = test_case.planned_distance * spec["fixed_price_per_km"]
    if test_case.plan == "minute":
        if test_case.type == "budget":
            price = test_case.time * spec["budget_price_per_min"]
        elif test_case.type == "luxury":
            price = test_case.time * spec["luxury_price_per_min"]

    if test_case.inno_discount == "yes":
        return price * (1 - spec["inno_discount"] / 100)
    elif test_case.inno_discount == "no":
        return price


test_cases: List[TestCase] = [
    TestCase(
        distance=-1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="budget",
        plan="minute",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=-1,
        time=1,
        planned_time=1,
        type="budget",
        plan="minute",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=-1,
        planned_time=1,
        type="budget",
        plan="minute",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=-1,
        type="budget",
        plan="minute",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="nonsense",
        plan="minute",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="budget",
        plan="nonsense",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="budget",
        plan="minute",
        inno_discount="nonsense",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="luxury",
        plan="fixed_price",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="budget",
        plan="minute",
        inno_discount="no",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="budget",
        plan="minute",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="budget",
        plan="fixed_price",
        inno_discount="no",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="budget",
        plan="fixed_price",
        inno_discount="yes",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="luxury",
        plan="minute",
        inno_discount="no",
    ),
    TestCase(
        distance=1,
        planned_distance=1,
        time=1,
        planned_time=1,
        type="luxury",
        plan="minute",
        inno_discount="yes",
    ),
]


failed = 0
for idx, test_case in enumerate(test_cases, 1):
    print(f"Running test case {idx} / {len(test_cases)}", end="\r")
    data = asdict(test_case)
    response = requests.get(PRICE_API_URL, params=data)

    expected_result = expected_price(test_case)

    try:
        result = response.json()["price"]
    except JSONDecodeError:
        result = response.text

    if result != expected_result:
        logging.error(
            f'For test case #{idx} expected "{expected_result}" but got "{result}" for {test_case}'
        )
        failed += 1

print()
print(f"Ran {len(test_cases)} tests. {failed} tests failed.")
