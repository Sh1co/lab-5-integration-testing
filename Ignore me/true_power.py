import re
import logging
from json.decoder import JSONDecodeError
from dataclasses import dataclass, asdict, fields, replace
from typing import Dict, Literal, List, Union

import requests

API_KEY = "AKfycbxReuav_j4FMBmT7i6ozd5E9oVjBOC7pKFyp5VlKjTRqmn7hjoeTDuzSayzMzTKKi2W"
EMAIL = "s.nafee@innopolis.university"
BASE_URL = f"https://script.google.com/macros/s/{API_KEY}/exec"
SPEC_URL = BASE_URL + f"?service=getSpec&email={EMAIL}"
PRICE_API_URL = BASE_URL + f"?service=calculatePrice&email={EMAIL}"
print(SPEC_URL)
# exit()
logging.basicConfig()


@dataclass
class Spec:
    budget_price_per_min: int
    luxury_price_per_min: int
    fixed_price_per_km: int
    allowed_deviation: int
    inno_discount: int


spec_pattern = re.compile(
    r"""Here is InnoCar Specs:
Budet car price per minute = (?P<budget_price_per_min>\d+)
Luxury car price per minute = (?P<luxury_price_per_min>\d+)
Fixed price per km = (?P<fixed_price_per_km>\d+)
Allowed deviations in % = (?P<allowed_deviation>\d+)
Inno discount in % = (?P<inno_discount>\d+)
"""
)

response = requests.get(SPEC_URL)
matches = spec_pattern.search(response.text).groupdict()
spec_dict = {field.name: field.type(matches[field.name]) for field in fields(Spec)}
spec = Spec(**spec_dict)
print("Spec:", spec)


@dataclass
class TestCase:
    type: Literal["budget", "luxury", "nonsense"]
    plan: Literal["minute", "fixed_price", "nonsense"]
    distance: int
    planned_distance: int
    time: int
    planned_time: int
    inno_discount: Literal["yes", "no", "nonsense"]


def expected_price(test_case) -> Union[Literal["Invalid Request"], int]:
    if test_case[0] not in ["budget", "luxury"]:
        return "Invalid Request"
    if test_case[1] not in ["minute", "fixed_price"]:
        return "Invalid Request"
    if test_case[1] == "fixed_price" and test_case[0] != "budget":
        return "Invalid Request"
    if test_case[2] <= 0:
        return "Invalid Request"
    if test_case[3] <= 0:
        return "Invalid Request"
    if test_case[4] <= 0:
        return "Invalid Request"
    if test_case[5] <= 0:
        return "Invalid Request"
    if test_case[6] not in ["yes", "no"]:
        return "Invalid Request"

    if test_case[1] == "fixed_price":
        deviation_dist = (test_case[3] - test_case[2]) / test_case[3] * 100
        deviation_time = (test_case[5] - test_case[4]) / test_case[5] * 100
        if (
            deviation_dist > spec.allowed_deviation
            or deviation_time > spec.allowed_deviation
        ):
            test_case = replace(test_case, plan="minute")
        else:
            price = test_case[3] * spec.fixed_price_per_km
    if test_case[1] == "minute":
        if test_case[0] == "budget":
            price = test_case[4] * spec.budget_price_per_min
        elif test_case[0] == "luxury":
            price = test_case[4] * spec.luxury_price_per_min

    if test_case[6] == "yes":
        return price * (1 - spec.inno_discount / 100)
    elif test_case[6] == "no":
        return price


test_cases = []

domain = [
    ["budget", "luxury", "nonsense"],
    ["minute", "fixed_price", "nonsense"],
    [10, -10],
    [10, -10],
    [10, -10],
    [10, -10],
    ["yes", "no", "nonsense"],
]


def gen_test_cases(ind, invalid, test_case):
    if ind == 7:
        test_cases.append(test_case)
        return
    for i in range(len(domain[ind]) - 1):
        tmp_test_case = test_case.copy()
        tmp_test_case.append(domain[ind][i])
        gen_test_cases(ind + 1, invalid, tmp_test_case)
    if not invalid:
        tmp_test_case = test_case.copy()
        tmp_test_case.append(domain[ind][len(domain[ind]) - 1])
        gen_test_cases(ind + 1, True, tmp_test_case)


gen_test_cases(0, False, [])

print(len(test_cases))


failed = 0
for idx, test_case in enumerate(test_cases, 1):
    print(f"Running test case {idx} / {len(test_cases)}", end="\r")
    dc_test_case = TestCase(
        test_case[0],
        test_case[1],
        test_case[2],
        test_case[3],
        test_case[4],
        test_case[5],
        test_case[6],
    )
    data = asdict(dc_test_case)
    response = requests.get(PRICE_API_URL, params=data)

    expected_result = expected_price(test_case)

    try:
        result = response.json()["price"]
    except JSONDecodeError:
        result = response.text

    if result != expected_result:
        logging.error(
            f'Expected "{expected_result}" but got "{result}" for {test_case}'
        )
        failed += 1

print()
print(f"Ran {len(test_cases)} tests. {failed} tests failed.")
