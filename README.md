# Lab5 -- Integration testing

## BVA

Parameter list:

| Parameter          | Equivalence Class                   |
|--------------------|-------------------------------------|
| `type`             | `budget`; `luxury`; nonsense        |
| `plan`             | `minute`; `fixed_price`; nonsense   |
| `distance`         | `<=0`; `>0`                         |
| `planned_distance` | `<=0`; `>0`                         |
| `time`             | `<=0`; `>0`                         |
| `planned_time`     | `<=0`; `>0`                         |
| `inno_discount`    | `yes`; `no`; nonsense               |

## Decision table

| Parameters          | Values                              |  1   |  2   |  3   |  4   |     5     |     6     |     7     |      8       |    9    |   10    |      11      |      12      |   13    |   14    |
| ------------------- | ----------------------------------- |:-----:|:-----:|:-----:|:-----:|:----------:|:----------:|:----------:|:-------------:|:---------:|:-------------:|:-------------:|:--------:|:--------:|:--------:|
| `distance`          | `>0`; `<=0`                         | `<=0` | `>0`  | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `planned_distance`  | `>0`; `<=0`                         |   *   | `<=0` | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `time`              | `>0`; `<=0`                         |   *   |   *   | `<=0` | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `planned_time`      | `>0`; `<=0`                         |   *   |   *   |   *   | `<=0` |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `type`              | `budget`, `luxury`, `nonsense`      |   *   |   *   |   *   |   *   | `nonsense` |     *      |     *      |   `luxury`    | `budget` | `budget` |   `budget`    |   `budget`    | `luxury` | `luxury` |
| `plan`              | `minute`, `fixed_price`, `nonsense` |   *   |   *   |   *   |   *   |     *      | `nonsense` |     *      | `fixed_price` | `minute` | `minute` | `fixed_price` | `fixed_price` | `minute` | `minute` |
| `inno_discount`     | `yes`, `no`, `nonsense`             |   *   |   *   |   *   |   *   |     *      |     *      | `nonsense` |       *       |  `no`    |  `yes`   |     `no`      |     `yes`     |   `no`   |  `yes`   |
| **Request Validity**    |                                     |   ❌   |   ❌   |   ❌   |   ❌   |     ❌      |     ❌      |     ❌      |       ❌       |    ✔️     |    ✔️     |       ✔️       |       ✔️       |     ✔️    |    ✔️     |

## Found bugs

My specs (s.nafee@innopolis.university):

```txt
Budet car price per minute = 30
Luxury car price per minute = 83
Fixed price per km = 16
Allowed deviations in % = 8
Inno discount in % = 10
```

**Using the script [api_tester.py](./api_tester.py), the following bugs were discovered**

### Bugs

1. **Budget Type** calculation is not working properly:
    - `For test case #9 expected "30" but got "21" for TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='no')`
    - `For test case #10 expected "27.0" but got "21" for TestCase(type='budget', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes')`
    - `For test case #11 expected "16" but got "12.5" for TestCase(type='budget', plan='fixed_price', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='no')`
    - `For test case #12 expected "14.4" but got "12.5" for TestCase(type='budget', plan='fixed_price', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes')`
2. **Inno Discount** is not correctly applied:
    - `For test case #14 expected "74.7" but got "83" for TestCase(type='luxury', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes')`
3. **Fixed Price** can't be tested because it can't be tested independently
    - Since the fixed price option is only available for budget type and budget type is broken, fixed price functionality couldn't be validated.
